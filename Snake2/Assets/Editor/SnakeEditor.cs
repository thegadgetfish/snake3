﻿using System;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SnakeEnemy))]
public class SnakeEditor:Editor
{
    private void OnSceneGUI()
    {
        SnakeEnemy snake = (SnakeEnemy) target;
        Handles.color = Color.white;
        Handles.DrawWireArc(snake.transform.position, Vector3.up, Vector3.forward, 360, snake.m_detectRadius);
        // both sides of view field
        Vector3 viewAngle01 = DirectionFromAngle(snake.transform.eulerAngles.y, -snake.m_detectAngle / 2);
        Vector3 viewAngle02 = DirectionFromAngle(snake.transform.eulerAngles.y, snake.m_detectAngle / 2);

        Handles.color = Color.yellow;
        Handles.DrawLine(snake.transform.position, snake.transform.position + viewAngle01 * snake.m_detectRadius);
        Handles.DrawLine(snake.transform.position, snake.transform.position + viewAngle02 * snake.m_detectRadius);
    }

    private Vector3 DirectionFromAngle(float eulerY, float angleInDegrees)
    {
        angleInDegrees += eulerY;
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }
}
