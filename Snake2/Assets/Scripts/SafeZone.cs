using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeZone : MonoBehaviour
{
		[SerializeField] public GameObject m_activeEffect;

		private GameObject m_pooledVfx = null;

		void OnTriggerEnter(Collider collision)
		{

			Debug.Log("On Trigger Enter");
			var curTransf = gameObject.transform.position;

			if(m_pooledVfx == null)
				m_pooledVfx = Instantiate(m_activeEffect, gameObject.transform);

			m_pooledVfx.transform.localPosition = new Vector3(curTransf.x, curTransf.y + 2, curTransf.z);
			m_pooledVfx.SetActive(true);
		}

		void OnTriggerExit(Collider c)
		{
			if(m_pooledVfx != null)
				m_pooledVfx.SetActive(false);
        }
}
