
using UnityEngine;

public class SnakeEnemy : MonoBehaviour
{
    [SerializeField] public GameObject[] m_segments;
    [SerializeField] public float m_speed;
    [SerializeField] public float m_minDistance;
    [SerializeField] public int m_patrolRange;
    [SerializeField] public Transform m_player;
    
    [SerializeField] public float m_detectRadius;
    [Range(0, 360)]
    [SerializeField] public float m_detectAngle;
    
    private float m_degrees = 0;
    private float m_timer = 0;
    private SnakeMovement m_curMovement;
    private float m_startRotation;
    private bool m_playerDetected = false;
    private LineRenderer m_lineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        m_curMovement = SnakeMovement.forward;
    }

    private void Turn(SnakeMovement newMovement)
    {
        if (newMovement != m_curMovement)
        {
            m_curMovement = newMovement;
            m_startRotation = m_segments[0].transform.rotation.y;
            m_startRotation = 0;
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        Transform head = m_segments[0].transform;
        if (IsPlayerDetected())
        {
            ChangeColor(true);
            // temp for now, pause everything
            // rotate head to look at player
            if(m_lineRenderer==null)
                m_lineRenderer = gameObject.AddComponent<LineRenderer>();
            m_lineRenderer.enabled = true;
            m_lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
            m_lineRenderer.widthMultiplier = .2f;
            m_lineRenderer.SetPosition(0, head.position);
            m_lineRenderer.SetPosition(1, m_player.position);
            return;
        }

        if (m_lineRenderer != null)
            m_lineRenderer.enabled = false;
        ChangeColor(false);
        m_timer++;
        
        
        // Only turn if snake isn't already turning
        if (head.position.z >= m_patrolRange || head.position.z <= -m_patrolRange)
        {
            if (m_curMovement != SnakeMovement.left && m_curMovement != SnakeMovement.right)
            {
                // Do some random stuff here. For now, turn left
                Turn(SnakeMovement.left);
            }
        }

        switch (m_curMovement)
        {
            // don't do anything
            case SnakeMovement.forward:
                break;
            case SnakeMovement.right:
                if (m_timer >= 10)  // Rotate 1 degree every 10 ticks
                {
                    head.transform.Rotate(0, 1, 0);
                    m_startRotation++;
                    m_timer = 0;
                }
                
                break;
            case SnakeMovement.left:
                if (m_timer >= 10)
                {
                    head.transform.Rotate(0, -1, 0);
                    m_startRotation++;
                    m_timer = 0;
                }
                
                break;
        }

        if (m_startRotation > 10)
        {
            m_curMovement = SnakeMovement.forward;
        }

        head.Translate(Vector3.forward * Time.deltaTime * m_speed);

        for (int i = 1; i < m_segments.Length; i++) {

            Transform curSegment = m_segments[i].transform;
            Transform prevSegment = m_segments[i - 1].transform;

            float dis = Vector3.Distance(prevSegment.position, curSegment.position);

            Vector3 newpos = prevSegment.position;

            newpos.y = head.position.y;

            float T = Time.deltaTime * dis / m_minDistance * m_speed;
            
            if (T > 0.5f)
                T = 0.5f;
            curSegment.position = Vector3.Slerp(curSegment.position, newpos, T);
            curSegment.rotation = Quaternion.Slerp(curSegment.rotation, prevSegment.rotation, T);

        }
    }

    private bool IsPlayerDetected()
    {
        Transform head = m_segments[0].transform;
        LayerMask mask = LayerMask.GetMask("PlayerLayer");
        Collider[] rangeChecks = Physics.OverlapSphere(head.position, m_detectRadius, mask);
        // Found something
        if (rangeChecks.Length != 0)
        {
            Transform firstTarget = rangeChecks[0].transform;
            Vector3 directionToTarget = (firstTarget.position - head.position).normalized;
            Debug.Log("Direction to target " + directionToTarget);
            if (Vector3.Angle(head.forward, directionToTarget) < m_detectAngle / 2)
            {
                float curDistance = Vector3.Distance(transform.position, m_player.position);
                // Found Player
                if (!Physics.Raycast(head.position, directionToTarget, curDistance))
                {
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    void ChangeColor(bool found)
    {
        for (int i = 0; i < m_segments.Length; i++)
        {
            Renderer snakeRenderer =  m_segments[i].GetComponent<Renderer>();
            if (found)
            {
                snakeRenderer.material.SetColor("_Color", Color.red);
            }
            else
            {
                snakeRenderer.material.SetColor("_Color", Color.white);
            }
        }
    }
}
