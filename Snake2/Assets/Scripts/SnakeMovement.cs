﻿public enum SnakeMovement
{
    forward = 0,
    left = 1,
    right = 2
}
